import math
import json
import re
from urllib.request import Request, urlopen

def distance(p0, p1):
    return math.sqrt((p0[0] - p1[0])**2 + (p0[1] - p1[1])**2)

path_to_save = '' # create a directory and write the name of directory here
#response = urlopen('https://github.com/NREL/EnergyPlus/raw/develop/weather/master.geojson')
data = json.loads(open('lookup_file.json').read())
count = 0
cooList = list()
for location in data.get('features'):
    x = location.get('geometry').get('coordinates')
    tupY = (x[0], x[1])
    cooList.append(tupY)
print(len(cooList))

coordinate = (18.6702698, -19.113323)
nearest = min(cooList, key=lambda x: distance(x, coordinate))
lookup_key = {'coordinates': list(nearest)}
for location in data.get('features'):
    x = location.get('geometry').get('coordinates')
    if x == list(nearest):
        match = re.search(r'href=[\'"]?([^\'" >]+)', location['properties']['epw'])
        if match:
            url = match.group(1)
            name = url[url.rfind('/') + 1:]
            count += 1
            print(count, ':', name, '\t')
            response = Request(url, headers={'User-Agent': "Magic Browser"})
            with open(path_to_save + name, 'wb') as f:
                f.write(urlopen(response).read())
print('done!')